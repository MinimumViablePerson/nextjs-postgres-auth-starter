import { revalidatePath } from 'next/cache'
import { redirect } from 'next/navigation'
import prisma from '../lib/prisma'

export default async function Home () {
  const users = await prisma.user.findMany()

  async function createUser (formData: FormData) {
    'use server'

    const existingUser = await prisma.user.findUnique({
      where: {
        email: String(formData.get('email'))
      }
    })

    if (existingUser) return redirect('/already-registered')

    await prisma.user.create({
      data: {
        email: String(formData.get('email')),
        password: 'password'
      }
    })

    revalidatePath('/')

    return redirect('/thank-you')
  }

  return (
    <div className='h-screen p-3 grid content-start gap-3'>
      <h1 className='text-emerald-600 text-center'>RSVP Example!</h1>
      <form action={createUser} className='grid text-black gap-2'>
        <input
          type='email'
          name='email'
          required
          className='px-3 py-2 rounded border'
          placeholder='Enter your email here...'
        />
        <button className='bg-emerald-600 text-white px-4 py-2 rounded'>
          REGISTER
        </button>
      </form>

      <p>Below are the emails that have already been registered.</p>
      <p>
        Depending on whether you are already registered or not, you will land on
        the <b className='text-emerald-500'>Already registered</b> page, or the{' '}
        <b className='text-emerald-500'>Thank you</b> page.
      </p>
      <ul>
        {users.map(user => (
          <li key={user.id}>
            <h2>{user.email}</h2>
          </li>
        ))}
      </ul>
    </div>
  )
}
