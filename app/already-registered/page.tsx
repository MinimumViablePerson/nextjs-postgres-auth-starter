export default function AlreadyRegistered () {
  return (
    <div className='h-screen grid place-content-center text-center'>
      <h1 className='text-xl text-emerald-500'>You are already registered!</h1>
      <h2 className='text-xl text-emerald-500'>See you there 👋</h2>
    </div>
  )
}
