export default function ThankYou () {
  return (
    <div className='h-screen grid place-content-center text-center'>
      <h1 className='text-xl text-emerald-500'>Thank you for registering!</h1>
      <h2 className='text-xl text-emerald-500'>See you there 👋</h2>
    </div>
  )
}
